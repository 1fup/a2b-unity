#A2B the Unity edition

This is yet another remake of my A2B game in yet another technology.

##Prerequisites

Unity3D Version 4.2

Check here for the [download]:

[download]: http://unity3d.com/unity/download/

Credits:

Football model:

Downloaded from: http://www.blendswap.com/blends/view/2776
By: http://www.blendswap.com/user/B65ISP

American football:
Downloaded from: http://www.blendswap.com/blends/view/17718
By: http://www.blendswap.com/user/kc8qzo

Beachball model:
Downloaded from: http://www.blendswap.com/blends/view/45572
By: http://www.blendswap.com/user/dotcom461

Bolas tennis ball model:
Downloaded from: http://www.blendswap.com/blends/view/69473
By: http://www.blendswap.com/user/Rossclark

Pool ball models:
Downloaded from:http://www.blendswap.com/blends/view/69272
By: http://www.blendswap.com/user/blenderednelb
