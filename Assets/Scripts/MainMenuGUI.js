﻿#pragma strict

var gameManager : GameManager;

var guiSkin: GUISkin;
var nativeVerticalResolution = 1200.0;

function Start() {
	gameManager = FindObjectOfType(GameManager);
}

function OnGUI() {

	// Set up gui skin
	GUI.skin = guiSkin;

	if (GUI.Button (Rect(Screen.width/2-100,Screen.height/2-150,200,60), "Start") ) {
		
		if(gameManager != null) {
			gameManager.StartNewGame();			
			gameManager.LoadNextLevel();
		 } else {
		 	Debug.LogError("GameManager not found");
		 }
	}
	
	var isWebPlayer = (Application.platform == RuntimePlatform.OSXWebPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer);
	if (!isWebPlayer) {
		//if (GUI.Button( Rect( (Screen.width/2)-70, Screen.height - 80, 140, 70), "Quit")) {
		if (GUI.Button (Rect(Screen.width/2-100,Screen.height/2+100,200,60), "Quit") ) {
			Application.Quit();
		}
	}
}