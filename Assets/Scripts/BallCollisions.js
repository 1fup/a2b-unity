#pragma strict

var gameManager : GameManager;

function Start() {
	gameManager = FindObjectOfType(GameManager);
	if(gameManager == null) {
	 	Debug.LogError("GameManager not found");
	}
}


function OnTriggerEnter (other : Collider) {
	Debug.Log("Collision with:" + other.tag);
	switch(other.tag) {
		case "EndBox":
	   		// load next level
		 	gameManager.LoadNextLevel();
	   		break;
		case "DeathPlane":
			Debug.LogError("Player died!");
			gameManager.PlayerDied();
			break;

	}

/*
function OnCollisionEnter (collision : Collision) {
	switch(other.tag) {
		case "EndBox":
	   		// load next level
		 	gameManager.LoadNextLevel();
	   		break;
		case "DeathPlane":
			Debug.LogError("Player died!
			gameManager.PlayerDied();
			break;

	}
*/
			
	
}