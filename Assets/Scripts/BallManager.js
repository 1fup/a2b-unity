﻿#pragma strict

public var LevelBalls : GameObject[] = new GameObject[5];

public var startWithBallId : int;
public var startBlock : GameObject;

private var currentBall : GameObject;
private var currentBallId : int;
private var MAX_BALL_ID : int = 4;

private var ballChangeDelay : int = 1; // time in seconds until ball change allowed again
private var ballChangeAllowed : boolean = true;

function Awake () {

	ballChangeAllowed = true;
	
	if(startWithBallId < 0 || startWithBallId > MAX_BALL_ID) {

		Debug.LogError("Invalid ball id:" + startWithBallId.ToString());

	} else {
		var rot : Quaternion = new Quaternion();
		var pos : Vector3 = startBlock.transform.position;
				
		pos.y += 2;

		currentBallId = startWithBallId;
		currentBall = Instantiate(LevelBalls[currentBallId], pos, rot);
		
		SendMessage ("SetCurrentBall", currentBall);	
	}


}

function GetCurrentBall() {
	return currentBall;
}

function Update () {

}

// Detects any keyboard event
function OnGUI() {
	var e : Event = Event.current;
	if (e.isKey && e.keyCode == KeyCode.Q){
		// switch to previous ball
		SelectPreviousBall();
	}
	if (e.isKey && e.keyCode == KeyCode.E){
		// switch to next ball
		SelectNextBall();
	}

}

function SelectPreviousBall() {

	if (ballChangeAllowed) {
		Debug.Log("Select previous ball");
		if(currentBallId >0) {
			var nextBallId = currentBallId -1;
		} else {
			nextBallId = MAX_BALL_ID;
		}
		if(LevelBalls[nextBallId] != null) {
			// replace current ball
			ReplaceCurrentBallWith(nextBallId);
		}
	}
}

function SelectNextBall() {
	if (ballChangeAllowed) {
		Debug.Log("Select next ball");
		if(currentBallId < MAX_BALL_ID) {
			var nextBallId = currentBallId +1;
		} else {
			nextBallId = 0;
		}
		if(LevelBalls[nextBallId] != null) {
			// replace current ball
			ReplaceCurrentBallWith(nextBallId);
		}
	}
}

function ReplaceCurrentBallWith(nextBallId : int) {

	var nextBall = Instantiate(LevelBalls[nextBallId], currentBall.transform.position, currentBall.transform.rotation);
	// copy velocity of object
	nextBall.rigidbody.angularVelocity = currentBall.rigidbody.angularVelocity;
	nextBall.rigidbody.velocity = currentBall.rigidbody.velocity;
	// apply to new ball
	Destroy(currentBall);
	currentBall = nextBall;
	currentBallId = nextBallId;

	StartCoroutine(InhibitBallChange());
	SendMessage ("SetCurrentBall", currentBall);	

}

function InhibitBallChange() {
	
	Debug.Log("Stop ball change working");
	ballChangeAllowed = false;
	
	yield WaitForSeconds(ballChangeDelay);

	Debug.Log("Start ball change working");
	ballChangeAllowed = true;

}