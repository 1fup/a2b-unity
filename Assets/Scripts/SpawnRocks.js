﻿#pragma strict

public var rockPrefab : Transform;

function OnCollisionEnter(collision : Collision) {
		// Rotate the object so that the y-axis faces along the normal of the surface
		var contact : ContactPoint = collision.contacts[0];
		//var rot : Quaternion = Quaternion.FromToRotation(Vector3.up, contact.normal);
		var rot : Quaternion = new Quaternion();
		var pos : Vector3 = contact.point;
		pos.y += 5;
		Instantiate(rockPrefab, pos, rot);
		//Destroy (gameObject);
}