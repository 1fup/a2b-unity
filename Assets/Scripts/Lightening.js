﻿#pragma strict

var lightening : Light;

function Start () {
	lightening = FindObjectOfType(Light);
}

function Update () {

	var intensity = (Random.value *100);

	if(intensity>95) {
		lightening.intensity = 8;
	} else {
		lightening.intensity = 0;
	}
}