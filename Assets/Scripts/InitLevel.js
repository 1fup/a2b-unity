#pragma strict

private var currentBall: GameObject;
var startBox: GameObject;
var levelStartTextLine1: String;
var levelStartTextLine2: String;
var displayText: boolean;
//var footballPrefab : Transform;


private var displayTextSeconds : int = 5;

private var guiStyle : GUIStyle;

function Start () {

	/*

	// start on box
	currentBall.transform.position.x = startBox.transform.position.x;
	currentBall.transform.position.y = startBox.transform.position.y + 3.0;
	currentBall.transform.position.z = startBox.transform.position.z;
	*/
	displayText = true;
		
	StartCoroutine("Fade");

}

function InitCurrentBall() {
	
	// find ball tagged
	var balls = GameObject.FindGameObjectsWithTag("CurrentBall");
	//
	for(var ball : GameObject in balls) {
		currentBall = ball;
		// attach camera to ball
		// attach controls to ball
		return;
	}

}
/*
function RemoveCurrentBall() {
	var balls : GameObject[];
	balls = gameObject.FindGameObjectsWithTag("CurrentBall");
	//
	for(var ball : GameObject in balls) {
		Destroy(ball);
	}
}

function AddCurrentBall() {
	// create instance of prefab
	var rot : Quaternion = new Quaternion();
	var pos : Vector3 = new Vector3();
		
	var currentBall = Instantiate(footballPrefab, pos, rot);
	
	gameObject.AddComponent(currentBall);

}
*/


function OnGUI() {

	if(displayText) {
		GUI.ModalWindow(1, new Rect(Screen.width/2-100,Screen.height/2-100,200,50), DrawDialog, "");
	}
}

function DrawDialog() {

	GUI.Label(new Rect(10,15,200,50),levelStartTextLine1);
	GUI.Label(new Rect(10,30,200,50),levelStartTextLine2);
}

function Fade() {
	for (var f = 0; f <= displayTextSeconds; f++) {
		yield WaitForSeconds(1);
	}
	displayText = false;
}

function Update () {

}