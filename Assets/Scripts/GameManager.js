﻿#pragma strict

class GameManager extends UnityEngine.MonoBehaviour

{   
	// constants
    public static final var TOTAL_LEVELS : int = 6;
    public static final var TOTAL_LIVES : int = 3;
    public static final var LEVEL : String = "Level";

    public static final var STATE_MAIN_MENU : String = "MainMenu";
    public static final var STATE_LEVEL_INTRO : String = "LevelIntro";
    public static final var STATE_LEVEL_PLAYING : String = "LevelPlaying";
    public static final var STATE_LEVEL_COMPLETED : String = "LevelCompleted";
    public static final var STATE_PLAYER_DIED : String = "PlayerDied";
    public static final var STATE_GAME_OVER : String = "GameOver";
    public static final var STATE_GAME_COMPLETED : String = "GameCompleted";
    
    public static final var SCENE_GAME_OVER : String = "GameOver";
    public static final var SCENE_GAME_COMPLETED : String = "GameCompleted";

    private var score : int;
    private var level : int =0;
    private var lives : int = TOTAL_LIVES;
    
    private var gameState : String = STATE_LEVEL_INTRO;
    
    
    
	function GetLives() {
		return this.lives;
	}

	function GetLevel() {
		return this.level;
	}
	
	function GetGameState() {
		return this.gameState;
	}
	
	function SetGameState(newState : String) {
		this.gameState = newState;
	}

	// start new game
    function StartNewGame() {
        score = 0;
        level = 0;
        lives = TOTAL_LIVES;
    }   
    
    function LoadNextLevel() {
    		level++;
    		
    		if(level > TOTAL_LEVELS) {
    			GameCompleted();
    		} else {
			 	SetGameState(STATE_LEVEL_INTRO);		
    			LoadLevel();
		 	}
    }

    function ReLoadLevel() {
		 	SetGameState(STATE_LEVEL_PLAYING);		   			
   			LoadLevel();
    }

	function LoadLevel() {
		var levelName : String;
	 	levelName = LEVEL + level.ToString();
	 	Application.LoadLevel(levelName);
	 	
    }


	function PlayerDied() {
			Debug.Log("Player died");
    		lives--;
    		if(lives==0) {
    			GameOver();
    		} else {
    			Respawn();
    		}
    }
    
    function GameCompleted() {
	 	Application.LoadLevel(SCENE_GAME_COMPLETED);    
    }

    function GameOver() {
	 	Application.LoadLevel(SCENE_GAME_OVER);        
    }
    
    function Respawn() {
		// just reload level again...
		ReLoadLevel();
    }
}

 

function Awake()

{   
    GameObject.DontDestroyOnLoad(this);
}