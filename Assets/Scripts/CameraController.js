﻿#pragma strict

var gameManager : GameManager;

public var startFlyByObject : GameObject;
private var endObject : GameObject;

var cameraOffset : Vector3;
private var mouseOrbitScript : MouseOrbit;
private var cameraControllerScript : CameraController;
private var ballManagerScript : BallManager;

function OnLevelWasLoaded(level : int) {
	InitObjectReferences();
	Debug.Log("CAMERA_CONTROLLER:Level loaded:" + level.ToString() + gameManager.GetGameState());
	initFlyByCameraUpdate();
	EnableCameraState();
	
}

function Start () {
	InitObjectReferences();
	Debug.Log("CAMERA_CONTROLLER:Level started:" + gameManager.GetGameState());
	initFlyByCameraUpdate();
	EnableCameraState();
}

function InitObjectReferences() {
	gameManager = FindObjectOfType(GameManager);
	mouseOrbitScript = gameObject.GetComponent(MouseOrbit);
	cameraControllerScript = gameObject.GetComponent(CameraController);
	ballManagerScript = gameObject.GetComponent(BallManager);
}

function EnableFlyByCamera() {
	mouseOrbitScript.enabled = false;	
	cameraControllerScript.enabled = true;
}

function EnableFollowCamera() {
	mouseOrbitScript.enabled = true;	
	cameraControllerScript.enabled = false;
}

function EnableCameraState() {
	var currentState = gameManager.GetGameState();

	switch(currentState) {
			case GameManager.STATE_LEVEL_INTRO :
				EnableFlyByCamera();
				break;
			default :
				EnableFollowCamera();
				break;
		}
}

function LateUpdate () {

	// update camera dependent on gameState
	var currentState = gameManager.GetGameState();
	
	switch(currentState) {
		case GameManager.STATE_LEVEL_INTRO :
			flyByCameraUpdate();
			break;
		default :
			//followPlayerCameraUpdate();
			break;
	
	}

}

// Movement speed in units/sec.
	var speed = 2.0;
	
	// Time when the movement started.
	var startTime: float;
	
	// Total distance between the markers.
	var journeyLength: float;
	var journeyTime: float;
	
function initFlyByCameraUpdate() {

	
	// Keep a note of the time the movement started.
	startTime = Time.time;
	endObject = ballManagerScript.GetCurrentBall();	
	// Calculate the journey length.
	journeyLength = Vector3.Distance(startFlyByObject.transform.position, endObject.transform.position);
	journeyTime = 5.0;
	cameraOffset = new Vector3(0,10,-17.32);

}

function flyByCameraUpdate() {
	    // The center of the arc
	    var center = (startFlyByObject.transform.position + endObject.transform.position) * 0.5;
	    // move the center a bit downwards to make the arc vertical
	    center -= Vector3(0,1,0);
	
	    // Interpolate over the arc relative to center
	    var riseRelCenter = startFlyByObject.transform.position - center;
	    var setRelCenter = endObject.transform.position - center;
	    
	    // The fraction of the animation that has happened so far is
	    // equal to the elapsed time divided by the desired time for
	    // the total journey.
	    //var fracComplete = (Time.time - startTime) / journeyTime;
	    var timeDiff = (Time.time - startTime);
	    var fracComplete = (timeDiff / journeyTime);
	    transform.position = Vector3.Slerp(riseRelCenter, setRelCenter, fracComplete);
	    transform.position += center;
	    transform.position += cameraOffset;
	    
	    // when end of flyby reached
		if (fracComplete >= 1) {
			//Debug.Log("before X:"+ transform.position.x + " Y:"+ transform.position.y + " Z:"+ transform.position.z);
			SendMessage ("FlyByComplete");
		}
	
}

function FlyByComplete() {
		// change state
		gameManager.SetGameState(gameManager.STATE_LEVEL_PLAYING);
		mouseOrbitScript.enabled=true;	
		cameraControllerScript.enabled = false;
}

function SetCurrentBall (currentBall : GameObject) {
	print ("CAMERA_CONTROLLER - set current ball");
	InitObjectReferences();
	mouseOrbitScript.target = currentBall.transform;
}



