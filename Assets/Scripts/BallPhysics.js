#pragma strict

function FixedUpdate() {
	    if(rigidbody != null) {
		    var speed = 40;
		    var torque = Vector3(Input.GetAxis("Vertical"), 0, -Input.GetAxis("Horizontal"));
		    torque = Camera.main.transform.TransformDirection(torque);
		    torque.y = 0;
		    rigidbody.AddTorque(torque.normalized*speed);
		}
}

/*
// Detects any keyboard event
function OnGUI() {
	var e : Event = Event.current;
	if (e.isKey && e.keyCode == KeyCode.Space){
		// jump ball
	    var force = Vector3(0, 3, 0);
	    rigidbody.AddRelativeForce(force,ForceMode.Impulse);
	}
}

*/